﻿using RestSharp;

//init REST client
var restClient = new RestClient("https://a.nook.exchange");
restClient.DefaultParameters.AddParameter(new HeaderParameter("Origin", "https://nook.exchange"));
restClient.DefaultParameters.AddParameter(new HeaderParameter("Referer", "https://nook.exchange"));

//handle menu
switch (PrintMenu())
{
    case 0:
        return;
    case 1:
        DumpProfile();
        return;
    case 2:
        DumpLists();
        return;
    case 3:
        DumpLogin();
        return;
}

int PrintMenu()
{
    Console.WriteLine("Select an operation to execute:");
    Console.WriteLine("1: dump profile data (no login required)");
    Console.WriteLine("2: dump list data (no login required)");
    Console.WriteLine("3: dump profile data and personal lists (login required)");
    Console.WriteLine("0: exit application");

    var key = -1;
    while (key == -1)
    {
        if (int.TryParse(Console.ReadLine(), out var val) && val is >= 0 and <= 3) key = val;
        else Console.WriteLine("Invalid input, try again.");
    }
    return key;
}

string GetNextFilename(string filename, string extension)
{
    var newName = $"{filename}.{extension}";
    var i = 1;

    while (File.Exists(newName))
    {
        newName = $"{filename}-{i++}.{extension}";
    }

    return newName;
}

void DumpProfile()
{
    //read username
    Console.Write("Enter username: ");
    var username = Console.ReadLine();

    //fetch profile data
    Console.Write("Fetching user profile data...");
    var task = restClient.ExecuteAsync(new RestRequest($"/users/{username}"));
    task.Wait();
    if (!task.Result.IsSuccessful)
    {
        Console.WriteLine("failed");
        Console.WriteLine("Please verify that the username exist.");
        return;
    }
    Console.WriteLine("done");

    //save result
    var filename = GetNextFilename("profile", "json");
    File.WriteAllText(filename, task.Result.Content);
    Console.WriteLine($"Profile data saved under: {Path.Combine(Directory.GetCurrentDirectory(), filename)}");
}

void DumpLists()
{
    //read list IDs
    Console.Write("Enter list ID (https://nook.exchange/l/<id>). You can enter multiple IDs as space-separated list: ");
    var lists = Console.ReadLine()?.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
    if (lists is null) return;

    foreach (var list in lists)
    {
        //fetch list data
        Console.Write($"Fetching \"{list}\" list data...");
        var task = restClient.ExecuteAsync(new RestRequest($"/item-lists/{list}"));
        task.Wait();
        if (!task.Result.IsSuccessful)
        {
            Console.WriteLine("failed");
            Console.WriteLine("Please verify that the list exist.");
            continue;
        }
        Console.WriteLine("done");

        //save result
        var filename = GetNextFilename($"list-{list}", "json");
        File.WriteAllText(filename, task.Result.Content);
        Console.WriteLine($"List data saved under: {Path.Combine(Directory.GetCurrentDirectory(), filename)}");
    }
}

void DumpLogin()
{
    //read username and password
    Console.Write("Enter username: ");
    var username = Console.ReadLine();
    Console.Write("Enter password: ");
    var password = Console.ReadLine();

    //get access token
    Console.Write($"Logging in as {username}...");
    var loginData = new Dictionary<string, string> {{"username", username}, {"password", password}};
    var task1 = restClient.ExecuteAsync(new RestRequest("/login", Method.Post).AddJsonBody(loginData));
    task1.Wait();
    if (!task1.Result.IsSuccessful)
    {
        Console.WriteLine("failed");
        Console.WriteLine("Please verify that username and password are correct.");
        return;
    }
    Console.WriteLine("done");

    //add token as authentication header
    var token = task1.Result.Cookies?[0].Value;
    restClient.AddDefaultParameter(new HeaderParameter("Authorization", $"Bearer {token}"));

    //save result
    var filename1 = GetNextFilename("profile", "json");
    File.WriteAllText(filename1, task1.Result.Content);
    Console.WriteLine($"Profile data saved under: {Path.Combine(Directory.GetCurrentDirectory(), filename1)}");

    //fetch lists data
    Console.Write("Fetching user lists data...");
    var task2 = restClient.ExecuteAsync(new RestRequest("/@me/item-lists"));
    task2.Wait();
    if (!task2.Result.IsSuccessful)
    {
        Console.WriteLine("failed");
        Console.WriteLine("Please verify that the username exist.");
        return;
    }
    Console.WriteLine("done");

    //save result
    var filename2 = GetNextFilename("lists", "json");
    File.WriteAllText(filename2, task2.Result.Content);
    Console.WriteLine($"Lists data saved under: {Path.Combine(Directory.GetCurrentDirectory(), filename2)}");

}